//
//  ViewController.h
//  Instagram Photo Search App
//
//  Created by Shereef Marzouk on 11/14/15.
//  Copyright © 2015 Shereef Marzouk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtTagName;

- (IBAction)searchAction:(UITextField *)sender forEvent:(UIEvent *)event;

@end

