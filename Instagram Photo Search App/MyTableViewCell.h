//
//  MyTableViewCell.h
//  Instagram Photo Search App
//
//  Created by Shereef Marzouk on 11/14/15.
//  Copyright © 2015 Shereef Marzouk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myImageView;
@property (weak, nonatomic) IBOutlet UILabel *myTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *myDetailTextLabel;

@end
