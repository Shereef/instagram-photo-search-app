//
//  PicturesTableViewController.h
//  Instagram Photo Search App
//
//  Created by Shereef Marzouk on 11/14/15.
//  Copyright © 2015 Shereef Marzouk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PicturesTableViewController : UITableViewController
@property NSString* tagName;
@property NSString* nextMaxId;
@property NSMutableArray* media;
@end
