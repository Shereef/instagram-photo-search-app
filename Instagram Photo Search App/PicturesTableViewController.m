//
//  PicturesTableViewController.m
//  Instagram Photo Search App
//
//  Created by Shereef Marzouk on 11/14/15.
//  Copyright © 2015 Shereef Marzouk. All rights reserved.
//

#import "PicturesTableViewController.h"
#import "UIImageView+WebCache.h"
#import "InstagramKit.h"
#import "MyTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "UIView+Toast.h"

@interface PicturesTableViewController () <MWPhotoBrowserDelegate>
@property NSMutableArray* photos;
@property BOOL isLoadingMore;
@property MWPhotoBrowser *browser;
@end

@implementation PicturesTableViewController
@synthesize tagName, media, photos, isLoadingMore, browser;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    isLoadingMore = NO;
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    self.title = tagName;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [media count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    
//
//    return cell;
    static NSString *MyIdentifier = @"cell";
    
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[MyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    InstagramMedia* mediaItem = [media objectAtIndex:indexPath.row];
    NSURL* url = [mediaItem thumbnailURL];
    // Here we use the new provided setImageWithURL: method to load the web image
    [cell.myImageView sd_setImageWithURL:url
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.myTextLabel.text = mediaItem.caption.text;
    cell.myDetailTextLabel.text = [NSString stringWithFormat:@"from: '%@'", mediaItem.user.username];
    
    if (indexPath.row + 1 == media.count) {
        [self loadMore];
    }
    return cell;
}/**/
- (void) loadMore {
    if (isLoadingMore)
        return;
    isLoadingMore = YES;
    [self.tableView makeToastActivity:CSToastPositionCenter];
    [self.browser.view makeToastActivity:CSToastPositionCenter];
    //NSLog(@"Event hit: %@", [sender text]);
    InstagramEngine *engine = [InstagramEngine sharedEngine];
    [engine getMediaWithTagName:tagName count:15 maxId:self.nextMaxId withSuccess:^(NSArray *newMedia, InstagramPaginationInfo *paginationInfo) {
        self.nextMaxId = paginationInfo.nextMaxId;
        NSMutableArray* allmedia = [NSMutableArray arrayWithArray:self.media];
        [allmedia addObjectsFromArray:newMedia];
        for (InstagramMedia* mediaItem in newMedia) {
            MWPhoto *photo;
            if (mediaItem.isVideo) {
                photo = [MWPhoto photoWithURL:mediaItem.standardResolutionImageURL];
                photo.videoURL = mediaItem.standardResolutionVideoURL;
                photo.isVideo = YES;
            } else {
                photo = [MWPhoto photoWithURL:mediaItem.standardResolutionImageURL];
            }
            photo.caption = mediaItem.caption.text;
            [photos addObject:photo];
        }
        self.media = allmedia;
        [self.tableView reloadData];
        [self.browser reloadData];
        //NSLog(@"%d: %@", [media count], media);
        [self.tableView hideToastActivity];
        [self.browser.view hideToastActivity];
        isLoadingMore = NO;
    } failure:^(NSError *error, NSInteger serverStatusCode) {
        //NSLog(@"%d: %@", serverStatusCode, error);
        [self.tableView hideToastActivity];
        [self.browser.view hideToastActivity];
        isLoadingMore = NO;
    }];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Create array of MWPhoto objects
    self.photos = [NSMutableArray array];
    
    
    for (InstagramMedia* mediaItem in media) {
        MWPhoto *photo;
        if (mediaItem.isVideo) {
            photo = [MWPhoto photoWithURL:mediaItem.standardResolutionImageURL];
            photo.videoURL = mediaItem.standardResolutionVideoURL;
            photo.isVideo = YES;
        } else {
            photo = [MWPhoto photoWithURL:mediaItem.standardResolutionImageURL];
        }
        photo.caption = mediaItem.caption.text;
        [photos addObject:photo];
    }
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Customise selection images to change colours if required
//    browser.customImageSelectedIconName = @"ImageSelected.png";
//    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:indexPath.row];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Question"
                                  message:@"Do you want to delete this photo ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if (index + 1 >= photos.count) {
                                 [photoBrowser setCurrentPhotoIndex:index - 1];
                             } else {
                                 [photoBrowser setCurrentPhotoIndex:index];
                             }
                             [media removeObjectAtIndex:index];
                             [self.tableView reloadData];
                             [photos removeObjectAtIndex:index];
                             [photoBrowser reloadData];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count) {
        MWPhoto* photo = [self.photos objectAtIndex:index];
        return photo;
    }
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    MWPhoto* photo = [self.photos objectAtIndex:index];
    if (photo.isVideo)
        [photoBrowser.view makeToast:@"Please note that videos may not work because I had no real devices (only simulators) to test with"];
    
    if (indexPath.row + 1 == media.count) {
        [self loadMore];
    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
