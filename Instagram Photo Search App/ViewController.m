//
//  ViewController.m
//  Instagram Photo Search App
//
//  Created by Shereef Marzouk on 11/14/15.
//  Copyright © 2015 Shereef Marzouk. All rights reserved.
//

#import "ViewController.h"
#import "InstagramKit.h"
#import "UIView+Toast.h"
#import "PicturesTableViewController.h"

@interface ViewController ()
@property NSString* maxId;
@property NSString* tagName;
@property NSArray* mediaArray;
@end


@implementation ViewController
@synthesize txtTagName, tagName, mediaArray, maxId;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchAction:(UITextField *)sender forEvent:(UIEvent *)event {
    [sender resignFirstResponder];
    NSString* incomingTag =[[sender text] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    if (![incomingTag isEqualToString:self.tagName]) {
        [self.view makeToastActivity:CSToastPositionCenter];
    //NSLog(@"Event hit: %@", incomingTag);
    InstagramEngine *engine = [InstagramEngine sharedEngine];
    [engine getMediaWithTagName:incomingTag count:15 maxId:nil withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        self.mediaArray = media;
        self.tagName = incomingTag;
        self.maxId = paginationInfo.nextMaxId;
        //NSLog(@"%d: %@", [media count], media);
        [self performSegueWithIdentifier:@"searchDone" sender:self];
    } failure:^(NSError *error, NSInteger serverStatusCode) {
        self.mediaArray = nil;
        self.tagName = nil;
        NSLog(@"%li: %@", (long)serverStatusCode, error);
        [self.view makeToast: [NSString stringWithFormat:@"Error obtaining tweets: (%@)", error.description]];
        [self.view hideToastActivity];
    }];
    } else {
        [self performSegueWithIdentifier:@"searchDone" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PicturesTableViewController* pics =  segue.destinationViewController;
    pics.media = [NSMutableArray arrayWithArray:mediaArray];
    pics.tagName = tagName;
    pics.nextMaxId = maxId;
    [self.view hideToastActivity];
}
@end
