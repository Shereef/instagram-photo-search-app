# README #

### Screen 1 ###
An application that will display pictures from Instagram using a Tag name chosen by the user (screen 1).√ 

When search is pressed the next view controller (tableview) will be pushed. √

### Screen 2 ###
Each Instagram picture will be displayed in a table view row.√

Each cell should display: 

* Image thumbnail.√
* caption text.√
* from: username.√

You can choose the cell layout.√

Back button returns to the search screen.√

When you select a table view row, a presented view controller will open displaying the selected photo as large as possible. √


### Screen 3 ###

* The photo is displayed full screen preserving the aspect ratio. √
* The user is able to use the pinch gesture to zoom in and out. √
* At the top of the screen, you can see a photo counter. √
* You can swipe (or use the arrows) to go to the next or previous picture. √
* The delete button will delete the picture from the current view and from the pictures array. This implies that after pressing the back button, the table view in the second view controller must be updated to take into account the deleted picture(s). √
* The info about the deleted pictures is lost after the user does a new search, but is preserved if he goes back and forth between view controllers 2 and 3. √
* The Back button dismisses the presented view, and the last viewed picture will remain highlighted in the table view (second view controller). √

### What is this repository for? ###

* An Interview

### How do I get set up? ###

* Clone / Download
* Run Instagram Photo Search App.xcworkspace
* Run on a simulator or real device

### Notes ###

* Videos May not work (I had no real devices to test with)